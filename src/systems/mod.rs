mod render;

pub use render::Render;
use legion::prelude::World;

pub trait System{
    fn run(&mut self, world: &mut World, _delta_time: f32);
}