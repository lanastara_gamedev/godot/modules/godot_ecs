use legion::prelude::{IntoQuery, Read, Write, World};
use super::System;

pub struct Render {
    canvas_item: gdnative::Rid,
}

use crate::components::{Position2, SpriteRender};

impl Render {
    pub fn new(canvas_item: gdnative::Rid) -> Self {
        Self { canvas_item }
    }
}

impl System for Render{
    fn run(&mut self, world: &mut World, _delta_time: f32)
    {
        let query = <(Read<Position2>, Write<SpriteRender>)>::query();

        for (pos, mut sprite) in query.iter_mut(world) {
            // godot_print!("rendering");
            let mut server = gdnative::VisualServer::godot_singleton();
            let rid = match sprite.canvas_item_id {
                Some(rid) => rid,
                None => {
                    let rid = server.canvas_item_create();
                    sprite.canvas_item_id = Some(rid);
                    server.canvas_item_add_texture_rect(
                        rid,
                        gdnative::Rect2::new(
                            gdnative::Point2::new(0.0, 0.0),
                            euclid::Size2D::new(32.0, 32.0),
                        ),
                        sprite.sprite,
                        false,
                        gdnative::Color::rgb(1.0, 1.0, 1.0),
                        false,
                        gdnative::Rid::default(),
                    );

                    server.canvas_item_set_parent(rid, self.canvas_item);
                    rid
                    
                }
            };

            let xform = euclid::Transform2D::create_translation(pos.x, pos.y);

            server.canvas_item_set_transform(rid, xform)
        }
    }
}