// use specs::prelude::*;

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Position2{
    pub x : f32,
    pub y : f32,
}

// impl Component for Position2{
//     type Storage = VecStorage<Self>;
// }