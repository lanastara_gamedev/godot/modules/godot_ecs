#[derive(Clone, Copy, Debug, PartialEq)]
pub struct SpriteRender{
    pub sprite: gdnative::Rid,
    pub(crate) canvas_item_id: Option<gdnative::Rid>,
}

impl SpriteRender{
    pub fn new(sprite_id: gdnative::Rid) -> Self{
        Self{
            sprite:sprite_id,
            canvas_item_id: None
        }
    }
}