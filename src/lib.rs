#[macro_use]
extern crate gdnative;

use legion::filter::{ChunksetFilterData, Filter};
use legion::prelude::*;
use legion::world::{IntoComponentSource, TagLayout, TagSet};

pub mod components;
pub mod systems;

pub mod prelude{
    pub use legion::prelude::*;
    pub use crate::systems::System;
}

#[derive(gdnative::NativeClass, Default)]
#[inherit(gdnative::Node)]
pub struct ECS {
    world: World,
    systems: Vec<Box<dyn crate::systems::System + 'static>>,
}

#[gdnative::methods]
impl ECS {
    fn _init(_owner: gdnative::Node) -> Self {
        godot_print!("Initializing ECS");
        Default::default()
    }

    #[export]
    fn _process(&mut self, mut _owner: gdnative::Node, delta: f32) {
        for system in self.systems.iter_mut() {
            system.run(&mut self.world, delta);
        }
    }
}

impl ECS {
    pub fn insert<T, C>(&mut self, tags: T, components: C) -> &[Entity]
    where
        C: IntoComponentSource,
        T: TagSet + TagLayout + for<'a> Filter<ChunksetFilterData<'a>>,
    {
        self.world.insert(tags, components)
    }

    pub fn register_system(&mut self, system: Box<(dyn crate::systems::System + 'static)>) {
        self.systems.push(system);
    }
}

pub fn get_ECS(owner: gdnative::Node) -> Option<gdnative::user_data::DefaultUserData<ECS>> {
    unsafe {
        let instance: Option<gdnative::Instance<ECS>> = owner
            .get_tree()
            .and_then(|tree| tree.get_root())
            .and_then(|root| root.get_node("Game/ECS".into()))
            .and_then(|node| gdnative::Instance::try_from_base(node));
        instance.map(|instance| instance.into_script())
    }
}

